from django.urls import path

from . import views

app_name = 'story0'

urlpatterns = [
    path('', views.index, name="index"),
]