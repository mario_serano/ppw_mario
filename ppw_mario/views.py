from django.shortcuts import render

def index(request):
    context = {
        "name":"Mario"
    }
    return render(request, "root/index.html", context)